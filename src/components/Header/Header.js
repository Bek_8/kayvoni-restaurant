import React from "react";
import { Nav, Kayvoni, LogoBack } from "./styled";
import { View } from "react-native";
import CustomStatusBar from "./CustomStatusBar";
import Logo from "../../../assets/img/logo.svg";
import { useNavigation } from "@react-navigation/native";
import ArrowBack from "../../../assets/img/arrowback.svg";
import { TouchableOpacity } from "react-native-gesture-handler";

const Header = ({ enableBack = false }) => {
  const navigation = useNavigation();
  return (
    <>
      <CustomStatusBar />
      <Nav>
        <View style={{ flex: 1, marginLeft: 10, justifyContent: "center" }}>
          {enableBack && (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <ArrowBack />
            </TouchableOpacity>
          )}
        </View>
        <Kayvoni>KAYVONI</Kayvoni>
        <View
          style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}
        >
          <LogoBack>
            <Logo />
          </LogoBack>
        </View>
      </Nav>
    </>
  );
};

export default Header;
