import styled from "styled-components";
import global from "../../../resources/global";

export const Nav = styled.View`
  background-color: ${global.colors.main};
  height: 60px;
  width: auto;
  flex-direction: row;
  align-items: center;
`;

export const LogoBack = styled.View`
  align-items: center;
  border: 1px solid ${global.colors.logoback};
  height: 34px;
  width: 34px;
  border-radius: 50px;
  background-color: ${global.colors.logoback};
  margin-right: 10px;
`;

export const Kayvoni = styled.Text`
  color: ${global.colors.white};
  font-size: 16px;
  line-height: 15.5px;
  font-weight: 700;
`;
