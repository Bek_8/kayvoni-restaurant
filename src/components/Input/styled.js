import styled from "styled-components";
import global from "../../../resources/global";

export const Input = styled.View`
  background-color: ${global.colors.white};
  width: ${global.strings.width - 20}px;
  height: ${global.strings.width / 10};
  justify-content: center;
  border-radius: 15px;
  margin: 5px 5px;
  padding-left: 10px;
`;
