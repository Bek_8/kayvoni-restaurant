import React from "react";
import { TextInput } from "react-native";
import { Input } from "./styled";

const Inputs = ({ value, onChangeText, secured = false }) => {
  return (
    <Input>
      <TextInput
        value={value}
        secureTextEntry={secured}
        onChangeText={(text) => onChangeText(text)}
      />
    </Input>
  );
};

export default Inputs;
