import styled from "styled-components";
import global from "../../../resources/global";

export const StatusView = styled.View`
  width: ${global.strings.width / 18}px;
  height: ${global.strings.width / 18}px;
  border-radius: 20px;
  border: 1px solid ${({ color = global.colors.red }) => color};
  background-color: ${({ color = global.colors.red }) => color};
`;
