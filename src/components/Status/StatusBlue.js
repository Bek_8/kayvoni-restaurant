import React from "react";
import { StatusView } from "./styled";
import global from "../../../resources/global";

const StatusYellow = () => {
  return <StatusView color={global.colors.blue}></StatusView>;
};

export default StatusYellow;
