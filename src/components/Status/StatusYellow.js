import React from "react";
import { StatusView } from "./styled";
import global from "../../../resources/global";

const StatusYellow = () => {
  return <StatusView color={global.colors.yellow}></StatusView>;
};

export default StatusYellow;
