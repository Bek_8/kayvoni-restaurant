import React from "react";
import { Top, TopFirst } from "./styled";
import global from "../../../resources/global";
import { Book14, Bold14 } from "../../../resources/palettes";

const Information = () => {
  return (
    <>
      <Top>
        <Bold14 color={global.colors.black}>Брон на дату: </Bold14>
        <TopFirst>
          <Book14 color={global.colors.black}>01.01.2020 </Book14>
          <Book14 color={global.colors.black}>14:40 </Book14>
        </TopFirst>
      </Top>
      <Top>
        <Bold14 color={global.colors.black}>Имя посетителя: </Bold14>
        <Book14 color={global.colors.black}>
          Mirali Tursuniy Mirali Tursuniy
        </Book14>
      </Top>
      <Book14 color={global.colors.secondaryText}>+998909000000</Book14>
    </>
  );
};

export default Information;
