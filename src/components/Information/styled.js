import styled from "styled-components";

export const Top = styled.View`
  justify-content: space-between;
  flex-direction: row;
  padding: 5px 5px;
`;

export const TopFirst = styled.View`
  flex-direction: row;
`;
