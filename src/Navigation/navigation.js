import React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import AuthScreen from "../../screens/Auth/AuthScreen";
import CalendarScreen from "../../screens/Calendar/CalendarScreen";
import CalendarInfo from "../../screens/Calendar/CalendarInfo";
import StatisticsScreen from "../../screens/Statistscs/StatisticsScreen";
import MenuScreen from "../../screens/Menu/MenuScreen";
import Header from "../components/Header/Header";
import global from "../../resources/global";
import { View } from "react-native";
import CalendarIcon from "../../assets/img/calendaricon.svg";
import MenuIcon from "../../assets/img/menuicon.svg";
import OrdersIcon from "../../assets/img/Orders.svg";
import ProfileIcon from "../../assets/img/ProfileIcon.svg";
import StatusOrderScreen from "../../screens/StatusOrder/StatusOrderScreen";
import StatusOrderInfo from "../../screens/StatusOrder/StatusOrderInfo";
import StatusAccept from "../../screens/StatusOrder/StatusAccept";
import StatusReject from "../../screens/StatusOrder/StatusReject";

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const tabsList = [
  {
    name: "Calendar",
    component: CalendarScreen,
    Icon: (props) => <CalendarIcon {...props} />,
  },
  {
    name: "Menu",
    component: MenuScreen,
    Icon: (props) => <MenuIcon {...props} />,
  },
  {
    name: "Status",
    component: StatusOrderScreen,
    Icon: (props) => <OrdersIcon {...props} />,
  },
  {
    name: "Statistics",
    component: StatisticsScreen,
    Icon: (props) => <ProfileIcon {...props} />,
  },
];

const renderTabs = () =>
  tabsList.map(({ name, component, Icon }) => (
    <Tab.Screen
      name={name}
      component={component}
      key={name}
      options={{
        tabBarIcon: ({ color, focused }) => (
          <View
            style={{
              backgroundColor: focused
                ? global.colors.white
                : global.colors.main,
              borderRadius: 50,
              padding: 4,
            }}
          >
            <Icon width={24} height={24} color={color} />
          </View>
        ),
      }}
    />
  ));

const BottomTabScreens = () => {
  return (
    <Tab.Navigator
      barStyle={{
        height: 70,
        backgroundColor: global.colors.main,
        borderTopColor: global.colors.white,
        borderTopWidth: 1,
      }}
      shifting={true}
      labeled={false}
      activeColor={global.colors.main}
      inactiveColor={global.colors.white}
      backBehavior={"none"}
    >
      {renderTabs()}
    </Tab.Navigator>
  );
};
export default function AppContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={"Auth"}>
        <Stack.Screen
          name="Auth"
          options={{ header: () => <Header /> }}
          component={AuthScreen}
        />
        <Stack.Screen
          name="Main"
          options={{ header: () => <Header /> }}
          component={BottomTabScreens}
        />
        <Stack.Screen
          name="CalendarInfo"
          options={{ header: () => <Header enableBack={true} /> }}
          component={CalendarInfo}
        />
        <Stack.Screen
          name="StatusOrderInfo"
          options={{ header: () => <Header enableBack={true} /> }}
          component={StatusOrderInfo}
        />
        <Stack.Screen
          name="StatusAccept"
          options={{ header: () => <Header enableBack={true} /> }}
          component={StatusAccept}
        />
        <Stack.Screen
          name="StatusReject"
          options={{ header: () => <Header enableBack={true} /> }}
          component={StatusReject}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
