// Auth
export const LOGIN = "LOGIN";
export const LOGIN_FAIL = "LOGIN_FAIL";

// Menu
export const MENU_SUCCESS = "MENU_SUCCESS";
export const MENU_FAIL = "MENU_FAIL";

// Order
export const ORDER_SUCCESS = "ORDER_SUCCESS";
export const ORDER_FAIL = "ORDER_FAIL";

// ShowOrder
export const SHOW_ORDER_SUCCESS = "SHOW_ORDER_SUCCESS";
export const SHOW_ORDER_FAIL = "SHOW_ORDER_FAIL";
