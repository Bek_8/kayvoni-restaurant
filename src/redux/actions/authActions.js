import { LOGIN_START, LOGIN_FAIL, LOGIN } from "../actiontypes/actiontypes";
import ApiService from "../../../src/services/api";
import { loaderStop, loaderStart } from "./loaderAction";

export const userLogin = ({ phone, password, navigation }) => (dispatch) => {
  loaderStart();
  const body = {
    phone,
    password,
  };
  return ApiService.postEvent("/auth", null, body)
    .then((value) => {
      console.log(value.accessToken);
      dispatch(loginSuccess({ phone, token: value.accessToken }));
      loaderStop();
    })
    .then(() => navigation.replace("Main"))
    .catch((error) => {
      loaderStop();
      dispatch(loginFail(error.message));
    });
};

export const loginSuccess = ({ phone, token }) => ({
  type: LOGIN,
  payload: { phone, token },
});

export const loginFail = (error) => ({
  type: LOGIN_FAIL,
  payload: error,
});

// export const ExitLogin = (user) => {
//   return {
//     type: LOGOUT,
//     user: null,
//   };
// };
