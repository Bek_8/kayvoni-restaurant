import * as loaderActionTypes from "../actiontypes/loaderActionTypes";

export const loaderStart = () => (dispatch) => {
  dispatch({
    type: loaderActionTypes.LOADING_START,
  });
};

export const loaderStop = () => (dispatch) => {
  dispatch({
    type: loaderActionTypes.LOADING_STOP,
  });
};
