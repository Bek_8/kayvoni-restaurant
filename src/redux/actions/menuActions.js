import ApiService from "../../services/api";
import { MENU_FAIL, MENU_SUCCESS } from "../actiontypes/actiontypes";
import { loaderStart, loaderStop } from "./loaderAction";

export const getMenu = (token) => (dispatch) => {
  loaderStart();
  return ApiService.getResources("/food", token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(menuSuccess(value.data));
        loaderStop();
      }
    })
    .catch((error) => {
      dispatch(menuFail(error));
      loaderStop();
    });
};

export function menuSuccess(data) {
  return {
    type: MENU_SUCCESS,
    payload: data,
  };
}

export function menuFail(error) {
  console.log("error", error);
  return {
    type: MENU_FAIL,
    payload: error,
  };
}
