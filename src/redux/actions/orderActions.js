import ApiService from "../../services/api";
import { ORDER_SUCCESS, ORDER_FAIL } from "../actiontypes/actiontypes";
import { loaderStart, loaderStop } from "./loaderAction";

export const getOrder = (token) => (dispatch) => {
  loaderStart();
  return ApiService.getResources("/orders", token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(orderSuccess(value.data));
        loaderStop();
      }
    })
    .catch((error) => {
      dispatch(orderFail(error));
      loaderStop();
    });
};

export function orderSuccess(data) {
  return {
    type: ORDER_SUCCESS,
    payload: data,
  };
}

export function orderFail(error) {
  console.log("error", error);
  return {
    type: ORDER_FAIL,
    payload: error,
  };
}
