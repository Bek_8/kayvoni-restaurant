import ApiService from "../../services/api";
import {
  SHOW_ORDER_FAIL,
  SHOW_ORDER_SUCCESS,
} from "../actiontypes/actiontypes";
import { loaderStart, loaderStop } from "./loaderAction";

export const getShowOrder = (token) => (dispatch) => {
  loaderStart();
  return ApiService.getResources(`show/order/${orderID}`, token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(showOrderSuccess(value.data));
        console.log(value.data);
        loaderStop();
      }
    })
    .catch((error) => {
      dispatch(showOrderFail(error));
      loaderStop();
    });
};

export function showOrderSuccess(data) {
  return {
    type: SHOW_ORDER_SUCCESS,
    payload: data,
  };
}

export function showOrderFail(error) {
  return {
    type: SHOW_ORDER_FAIL,
    payload: error,
  };
}
