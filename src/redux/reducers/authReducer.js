import { LOGIN, LOGIN_FAIL, LOGIN_START } from "../actiontypes/actiontypes";

const initialState = {
  token: null,
  phone: null,
  error: null,
};

export const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case LOGIN:
      const { phone, token } = payload;
      return { ...state, phone: phone, token: token };
    case LOGIN_FAIL:
      return {
        ...state,
        error: payload,
        phone: null,
      };
    default:
      return state;
  }
};
