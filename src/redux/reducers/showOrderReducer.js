import {
  SHOW_ORDER_FAIL,
  SHOW_ORDER_SUCCESS,
} from "../actiontypes/actiontypes";

const initialState = {
  data: [],
  error: null,
};

export function showOrderReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SHOW_ORDER_SUCCESS:
      return { ...state, data: payload };
    case SHOW_ORDER_FAIL:
      return { ...state, data: null, error: payload };
    default:
      return state;
  }
}
