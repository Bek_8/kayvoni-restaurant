import { MENU_FAIL, MENU_SUCCESS } from "../actiontypes/actiontypes";

const initialState = {
  data: [],
  error: null,
};

export function menuReducer(state = initialState, { type, payload }) {
  switch (type) {
    case MENU_SUCCESS:
      return { ...state, data: payload };
    case MENU_FAIL:
      return { ...state, data: null, error: payload };
    default:
      return state;
  }
}
