import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { menuReducer } from "./menuReducer";
import { loaderReducer } from "./loaderReducer";
import { orderReducer } from "./orderReducer";
import { showOrderReducer } from "./showOrderReducer";

export const rootReducer = combineReducers({
  auth: authReducer,
  menu: menuReducer,
  loading: loaderReducer,
  order: orderReducer,
  showOrder: showOrderReducer,
});
