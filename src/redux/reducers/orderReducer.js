import { ORDER_FAIL, ORDER_SUCCESS } from "../actiontypes/actiontypes";

const initialState = {
  data: [],
  error: null,
};

export function orderReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ORDER_SUCCESS:
      return { ...state, data: payload };
    case ORDER_FAIL:
      return { ...state, data: null, error: payload };
    default:
      return state;
  }
}
