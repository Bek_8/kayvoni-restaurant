import * as loaderActionTypes from "../actiontypes/loaderActionTypes";

const initialState = {
  loading: false,
};

export function loaderReducer(state = initialState, { type, payload }) {
  switch (type) {
    case loaderActionTypes.LOADING_START:
      return { loading: true };
    case loaderActionTypes.LOADING_STOP:
      return { loading: false };
    default:
      return state;
  }
}
