import React from "react";
import { Text } from "react-native";
import { Agenda } from "react-native-calendars";
import { TouchableOpacity } from "react-native-gesture-handler";
import CalendarItems from "./CalendarItems";

const CalendarScreen = ({ navigation }) => {
  const accepted = { key: "accepted", color: "red", selectedDotColor: "blue" };
  const deflect = { key: "deflect", color: "blue", selectedDotColor: "blue" };
  const waiting = { key: "waiting", color: "green" };
  return (
    <Agenda
      //   minDate={"2020-05-10"}
      //   hideExtraDays={true}
      //   enableSwipeMonths={true}
      //   markedDates={{
      //     "2020-11-16": {
      //       dots: [accepted, deflect, waiting],
      //     },
      //     "2020-11-18": { dots: [deflect, accepted] },
      //   }}
      //   markingType={"multi-dot"}
      items={{
        "2020-12-2": [{ name: "item 1 - any js object" }],
        "2020-12-3": [{ name: "item 2 - any js object" }],
      }}
      pastScrollRange={6}
      futureScrollRange={6}
      renderEmptyData={() => {
        return <Text>UPSSS</Text>;
      }}
      renderItem={(item) => {
        return (
          <TouchableOpacity onPress={() => navigation.navigate("CalendarInfo")}>
            <CalendarItems />
          </TouchableOpacity>
        );
      }}
    />
  );
};

export default CalendarScreen;
