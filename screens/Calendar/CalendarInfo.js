import React from "react";
import { Info, Middle, Top } from "./styled";
import { Bold14, Book12, Book14 } from "../../resources/palettes";
import global from "../../resources/global";
import Input from "../../src/components/Input/Inputs";
import { View } from "react-native";
import Information from "../../src/components/Information/Information";
import { TouchableWithoutFeedback, Keyboard } from "react-native";
import { getShowOrder } from "../../src/redux/actions/showOrderActions";

const CalendarInfo = () => {
  const dispatch = useDispatch();
  const { data, error, orderID } = useSelector((state) => state.showOrder);
  const { token } = useSelector((state) => state.auth);
  const { loading } = useSelector((state) => state.loading);

  useEffect(() => {
    dispatch(getShowOrder(token, orderID));
    console.log(orderID);
  }, []);

  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text> {error.message} </Text>;
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      {!!data.length &&
        data.order_item.map((item) => (
          <View>
            <Info>
              <Information />
              <Bold14 color={global.colors.black}>Примечание</Bold14>
              <Input />
              <Middle>
                <Book12 color={global.colors.black}>x1</Book12>
                <Book12 color={global.colors.black}>
                  Сет Miko love Сет Miko love (Большой)
                </Book12>
                <Book12 color={global.colors.black}>154 000 сум</Book12>
              </Middle>
              <Middle>
                <Book12 color={global.colors.black}>x2</Book12>
                <Book12 color={global.colors.black}>Количество персон</Book12>
                <Book12 color={global.colors.black}>54 000 сум</Book12>
              </Middle>
              <Top>
                <Bold14 color={global.colors.black}>Сумма саказа: </Bold14>
                <Book14 color={global.colors.black}>240 000 сум</Book14>
              </Top>
              <View
                style={{
                  borderBottomColor: global.colors.secondaryText,
                  borderBottomWidth: 1,
                }}
              />
              <Top>
                <Bold14 color={global.colors.black}>Для Kayvoni 10% </Bold14>
                <Book14 color={global.colors.black}>
                  {item.order_item.price_per_count} сум
                </Book14>
              </Top>
              <Top>
                <Bold14 color={global.colors.black}>Для ресторана % </Bold14>
                <Book14 color={global.colors.black}>340 000 сум</Book14>
              </Top>
              <Top>
                <Bold14 color={global.colors.black}>Оплачено </Bold14>
                <Book14 color={global.colors.black}>40 000 сум</Book14>
              </Top>
            </Info>
          </View>
        ))}
    </TouchableWithoutFeedback>
  );
};

export default CalendarInfo;
