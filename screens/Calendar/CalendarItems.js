import React from "react";
import { ItemBottom, ItemNumber, ItemWrap, Wrapper } from "./styled";
import {
  Bold14,
  Book14,
  SemiBold12,
  SemiBold18,
} from "../../resources/palettes";
import global from "../../resources/global";

const CalendarItems = () => {
  return (
    <Wrapper>
      <ItemWrap>
        <Bold14 color={global.colors.black}>
          Mirali Tursuniy Mirali Tursuniy
        </Bold14>
        <Book14 color={global.colors.secondaryText} style={{ paddingTop: 5 }}>
          +998909000000
        </Book14>
        <ItemBottom>
          <SemiBold12 color={global.colors.black} style={{ paddingTop: 5 }}>
            На 20 персон
          </SemiBold12>
          <SemiBold12
            color={global.colors.black}
            style={{ paddingTop: 5, marginLeft: 10 }}
          >
            Столик №123
          </SemiBold12>
        </ItemBottom>
      </ItemWrap>
      <ItemNumber>
        <SemiBold18 color={global.colors.black}>№01</SemiBold18>
      </ItemNumber>
    </Wrapper>
  );
};

export default CalendarItems;
