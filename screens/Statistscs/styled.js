import styled from "styled-components";
import global from "../../resources/global";

export const Top = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  margin: 5px;
  align-items: center;
`;

export const Title = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
`;

export const Bottom = styled.View`
  /* border: 1px solid ${global.colors.secondaryText};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  border-radius: 20px; */
  padding: 10px;
`;

export const Date = styled.View`
  flex-direction: column;
  width: ${global.strings.width}px;
`;
