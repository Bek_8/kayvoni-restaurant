import React, { useRef } from "react";
import { Bold14, Bold18, Book14 } from "../../resources/palettes";
import { Top, Title, Bottom, Date } from "./styled";
import global from "../../resources/global";
import Phone from "../../assets/img/phone.svg";
import { View, Image, Linking, Platform, Text, Button } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import DatePicker from "../../src/components/DatePicker/DatePicker";

const StatisticsScreen = () => {
  dialCall = (number) => {
    let phoneNumber = "";
    if (Platform.OS === "android") {
      phoneNumber = `tel:${number}`;
    } else {
      phoneNumber = `telprompt:${number}`;
    }
    Linking.openURL(phoneNumber);
  };

  const value = useRef();
  return (
    <>
      <Top>
        <Bold14 color={global.colors.black}>Поддержка:</Bold14>
        <View style={{ flex: 1, marginLeft: 10 }}>
          <Book14 color={global.colors.black}>998 90 800 00 00</Book14>
        </View>
        <TouchableOpacity
          onPress={() => {
            dialCall(908000000);
          }}
        >
          <Phone style={{ marginRight: 10 }} />
        </TouchableOpacity>
      </Top>
      <View
        style={{
          borderBottomColor: global.colors.secondaryText,
          borderBottomWidth: 2,
        }}
      />
      <Title>
        <Image source={global.images.restoran} />
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Bold18 color={global.colors.black}>Yapona mama Yapona mama</Bold18>
        </View>
      </Title>
      <Bottom>
        <Bold14 color={global.colors.black}>Выберите дату</Bold14>
        <Date>
          <DatePicker ref={value} />
        </Date>
      </Bottom>
    </>
  );
};

export default StatisticsScreen;
