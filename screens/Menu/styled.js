import styled from "styled-components";
import global from "../../resources/global";

export const Wrapper = styled.View`
  padding: 10px;
`;

export const WrapItem = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border: 1px solid ${global.colors.white};
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 50px;
  padding-right: 5px;
  margin: 5px;
`;

export const On = styled.View`
  justify-content: center;
  align-items: center;
  border: 1px solid ${global.colors.green};
  height: ${global.strings.width / 9}px;
  width: ${global.strings.width / 9}px;
  background-color: ${global.colors.green};
  border-radius: 50px;
`;

export const Off = styled.View`
  justify-content: center;
  align-items: center;
  border: 1px solid ${global.colors.secondaryText};
  height: ${global.strings.width / 9}px;
  width: ${global.strings.width / 9}px;
  background-color: ${global.colors.secondaryText};
  border-radius: 50px;
`;
