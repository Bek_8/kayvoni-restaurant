import React, { useEffect, useState } from "react";
import { WrapItem, Wrapper, On, Off } from "./styled";
import { Image, View, Text } from "react-native";
import global from "../../resources/global";
import { SemiBold12 } from "../../resources/palettes";
import SwitchOn from "../../assets/img/switchon.svg";
import SwitchOff from "../../assets/img/off.svg";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useDispatch, useSelector } from "react-redux";
import { getMenu } from "../../src/redux/actions/menuActions";

const Menu = () => {
  const [switchValue, setSwitchValue] = useState(false);
  const dispatch = useDispatch();
  const { data, error } = useSelector((state) => state.menu);
  const { token } = useSelector((state) => state.auth);
  const { loading } = useSelector((state) => state.loading);

  useEffect(() => {
    dispatch(getMenu(token));
  }, []);
  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text> {error.message} </Text>;
  return (
    <Wrapper>
      {!!data.length &&
        data.map((item) => (
          <WrapItem>
            <Image source={global.images.food} />
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <SemiBold12
                color={global.colors.black}
                style={{ textAlign: "center", maxWidth: "70%" }}
              >
                {item.name_ru}
              </SemiBold12>
            </View>

            <TouchableOpacity onPress={() => setSwitchValue((prev) => !prev)}>
              {switchValue ? (
                <Off>
                  <SwitchOff />
                </Off>
              ) : (
                <On>
                  <SwitchOn />
                </On>
              )}
            </TouchableOpacity>
          </WrapItem>
        ))}
    </Wrapper>
  );
};

export default Menu;
