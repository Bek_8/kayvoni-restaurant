import React, { useState } from "react";
import Information from "../../src/components/Information/Information";
import { Bold14 } from "../../resources/palettes";
import global from "../../resources/global";
import Input from "../../src/components/Input/Inputs";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ButtonReject } from "./styled";
import { Modal, View } from "react-native";
import Check from "./Check";

const StatusReject = ({ navigation }) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const handleSucess = () => {
    setModalVisible(true);
    setTimeout(() => navigation.navigate("Main"), 2000);
  };
  return (
    <>
      <Modal animationType="fade" visible={isModalVisible}>
        <View style={{ flex: 1 }}>
          <Check />
        </View>
      </Modal>
      <Information />
      <Bold14 color={global.colors.black}>Укажите причину отказа </Bold14>
      <Input />
      <TouchableOpacity onPress={() => handleSucess()}>
        <ButtonReject>
          <Bold14>Отправить</Bold14>
        </ButtonReject>
      </TouchableOpacity>
    </>
  );
};

export default StatusReject;
