import React, { useState } from "react";
import Information from "../../src/components/Information/Information";
import { Bold14 } from "../../resources/palettes";
import global from "../../resources/global";
import Input from "../../src/components/Input/Inputs";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Modal, View } from "react-native";
import { ButtonAccept } from "./styled";
import Check from "./Check";

const StatusAccept = ({ navigation }) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const handleSucess = () => {
    setModalVisible(true);
    setTimeout(() => navigation.replace("Main"), 2000);
  };
  return (
    <>
      <Modal animationType="fade" visible={isModalVisible}>
        <View style={{ flex: 1 }}>
          <Check />
        </View>
      </Modal>
      <Information />
      <Bold14 color={global.colors.black}>Укажите номер столика </Bold14>
      <Input />
      <TouchableOpacity onPress={() => handleSucess()}>
        <ButtonAccept>
          <Bold14>Отправить</Bold14>
        </ButtonAccept>
      </TouchableOpacity>
    </>
  );
};

export default StatusAccept;
