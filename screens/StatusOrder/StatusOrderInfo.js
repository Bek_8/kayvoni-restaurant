import React from "react";
import { View } from "react-native";
import { Accept, Buttons, Reject } from "./styled";
import { Middle, Top } from "../Calendar/styled";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SemiBold13, Bold14, Book12, Book14 } from "../../resources/palettes";
import Information from "../../src/components/Information/Information";
import global from "../../resources/global";
import Input from "../../src/components/Input/Inputs";

const StatusOrderInfo = ({ navigation }) => {
  return (
    <>
      <Information />
      <Bold14 color={global.colors.black}>Примечание </Bold14>
      <Input />
      <Middle>
        <Book12 color={global.colors.black}>x1</Book12>
        <Book12 color={global.colors.black}>
          Сет Miko love Сет Miko love (Большой)
        </Book12>
        <Book12 color={global.colors.black}>154 000 сум</Book12>
      </Middle>
      <Middle>
        <Book12 color={global.colors.black}>x2</Book12>
        <Book12 color={global.colors.black}>Количество персон</Book12>
        <Book12 color={global.colors.black}>54 000 сум</Book12>
      </Middle>
      <Top>
        <Bold14 color={global.colors.black}>Сумма саказа: </Bold14>
        <Book14 color={global.colors.black}>240 000 сум</Book14>
      </Top>
      <View
        style={{
          borderBottomColor: global.colors.secondaryText,
          borderBottomWidth: 1,
        }}
      />
      <Top>
        <Bold14 color={global.colors.black}>Для Kayvoni 10% </Bold14>
        <Book14 color={global.colors.black}>24 000 сум</Book14>
      </Top>
      <Top>
        <Bold14 color={global.colors.black}>Для ресторана % </Bold14>
        <Book14 color={global.colors.black}>340 000 сум</Book14>
      </Top>
      <Top>
        <Bold14 color={global.colors.black}>Оплачено </Bold14>
        <Book14 color={global.colors.black}>40 000 сум</Book14>
      </Top>
      <Buttons>
        <TouchableOpacity onPress={() => navigation.navigate("StatusReject")}>
          <Reject>
            <SemiBold13>Отклонить</SemiBold13>
          </Reject>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("StatusAccept")}>
          <Accept>
            <SemiBold13>Принять</SemiBold13>
          </Accept>
        </TouchableOpacity>
      </Buttons>
    </>
  );
};

export default StatusOrderInfo;
