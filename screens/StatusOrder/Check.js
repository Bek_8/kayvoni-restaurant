import React from "react";
import { CheckMark } from "./styled";
import Checker from "../../assets/img/check.svg";

const Check = () => {
  return (
    <CheckMark>
      <Checker />
    </CheckMark>
  );
};

export default Check;
