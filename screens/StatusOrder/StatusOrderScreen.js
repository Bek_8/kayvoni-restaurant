import React, { useEffect } from "react";
import { Book16 } from "../../resources/palettes";
import { Text, View } from "react-native";
import {
  Top,
  TopItem,
  ItemBottom,
  ItemNumber,
  ItemWrap,
  Wrapper,
  TitleStatus,
  First,
} from "./styled";
import global from "../../resources/global";
import {
  Book12,
  Bold14,
  Book14,
  SemiBold12,
  SemiBold18,
} from "../../resources/palettes";
import { useDispatch, useSelector } from "react-redux";
import { TouchableOpacity } from "react-native-gesture-handler";
import { getOrder } from "../../src/redux/actions/orderActions";
import StatusRed from "../../src/components/Status/StatusRed";
import StatusYellow from "../../src/components/Status/StatusYellow";
import StatusGreen from "../../src/components/Status/StatusGreen";
import StatusBlue from "../../src/components/Status/StatusBlue";

const StatusOrderScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const { data, error } = useSelector((state) => state.order);
  const { token } = useSelector((state) => state.auth);
  const { loading } = useSelector((state) => state.loading);

  useEffect(() => {
    dispatch(getOrder(token));
  }, []);

  return (
    <>
      <First>
        <Top>
          <TopItem>
            <StatusRed />
            <Book16 color={global.colors.black}>-отклонен</Book16>
          </TopItem>
          <TopItem>
            <StatusYellow />
            <Book16 color={global.colors.black}>-в ожидании</Book16>
          </TopItem>
        </Top>
        <Top>
          <TopItem>
            <StatusGreen />
            <Book16 color={global.colors.black}>-принят</Book16>
          </TopItem>
          <TopItem>
            <StatusBlue />
            <Book16 color={global.colors.black}>-закончен</Book16>
          </TopItem>
        </Top>
      </First>
      {!!data.length &&
        data.map((item) => (
          <View>
            <TouchableOpacity
              onPress={() => navigation.navigate("StatusOrderInfo")}
            >
              <Wrapper>
                <ItemWrap>
                  <TitleStatus>
                    <Bold14 color={global.colors.black}>
                      {item.user.name}
                    </Bold14>
                    <View>
                      {item.status == 2 ? (
                        <StatusGreen style={{ marginLeft: 5 }} />
                      ) : (
                        <StatusYellow />
                      )}
                    </View>
                  </TitleStatus>
                  <Book14
                    color={global.colors.secondaryText}
                    style={{ paddingTop: 5 }}
                  >
                    {item.user.phone}
                  </Book14>
                  <ItemBottom>
                    <SemiBold12
                      color={global.colors.black}
                      style={{ paddingTop: 5 }}
                    >
                      Цена предоплаты:
                    </SemiBold12>
                    <Book12
                      color={global.colors.black}
                      style={{ paddingTop: 5, marginLeft: 10 }}
                    >
                      {item.total_price} сум
                    </Book12>
                  </ItemBottom>
                  <ItemBottom>
                    <SemiBold12
                      color={global.colors.black}
                      style={{ paddingTop: 5 }}
                    >
                      Брон на дату:
                    </SemiBold12>
                    <Book12
                      color={global.colors.black}
                      style={{ paddingTop: 5, marginLeft: 10 }}
                    >
                      {item.reserve_time}
                    </Book12>
                  </ItemBottom>
                </ItemWrap>
                <ItemNumber>
                  <SemiBold18 color={global.colors.black}>
                    {item.table_number}
                  </SemiBold18>
                </ItemNumber>
              </Wrapper>
            </TouchableOpacity>
          </View>
        ))}
    </>
  );
};

export default StatusOrderScreen;
