import styled from "styled-components";
import global from "../../resources/global";

export const First = styled.View`
  flex-direction: row;
  justify-content: space-around;
`;

export const Top = styled.View`
  flex-direction: column;
  justify-content: flex-start;
  margin: 10px;
`;

export const TopItem = styled.View`
  flex-direction: row;
  margin: 5px;
`;

export const Wrapper = styled.View`
  flex-direction: row;
  margin: 10px;
`;

export const TitleStatus = styled.View`
  flex-direction: row;
`;

export const ItemWrap = styled.View`
  margin-top: 10px;
  padding: 10px;
  border-radius: 20px;
  border: 1px solid ${global.colors.white};
  background-color: ${global.colors.white};
  box-shadow: -10px 0px 10px rgba(0, 0, 0, 0.1);
  width: ${global.strings.width / 1.3}px;
  height: ${global.strings.width / 3.5}px;
`;

export const ItemNumber = styled.View`
  margin-top: 10px;
  border-radius: 100px;
  box-shadow: -10px 0px 10px rgba(0, 0, 0, 0.1);
  border: 1px solid ${global.colors.white};
  background-color: ${global.colors.white};
  box-shadow: -10px 0px 10px rgba(0, 0, 0, 0.1);
  width: ${global.strings.width / 3.5}px;
  height: ${global.strings.width / 3.5}px;
  align-items: center;
  justify-content: center;
  position: absolute;
  right: 15;
`;

export const ItemBottom = styled.View`
  flex-direction: row;
`;

export const Buttons = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Reject = styled.View`
  margin: 10px;
  border: 1px solid ${global.colors.red};
  background-color: ${global.colors.red};
  width: ${global.strings.width / 2.2}px;
  height: ${global.strings.width / 12}px;
  align-items: center;
  flex-direction: row;
  justify-content: center;
  border-radius: 10px;
`;

export const Accept = styled.View`
  margin: 10px;
  border: 1px solid ${global.colors.green};
  background-color: ${global.colors.green};
  width: ${global.strings.width / 2.2}px;
  height: ${global.strings.width / 12}px;
  align-items: center;
  flex-direction: row;
  justify-content: center;
  border-radius: 10px;
`;

export const ButtonAccept = styled.View`
  margin: 10px;
  width: ${global.strings.width / 1.1}px;
  height: ${global.strings.width / 12}px;
  border: 1px solid ${global.colors.green};
  background-color: ${global.colors.green};
  align-items: center;
  flex-direction: row;
  justify-content: center;
  border-radius: 10px;
`;

export const ButtonReject = styled.View`
  margin: 10px;
  width: ${global.strings.width / 1.1}px;
  height: ${global.strings.width / 12}px;
  border: 1px solid ${global.colors.red};
  background-color: ${global.colors.red};
  align-items: center;
  flex-direction: row;
  justify-content: center;
  border-radius: 10px;
`;

export const CheckMark = styled.View`
  width: ${global.strings.width}px;
  height: ${global.strings.height / 1.3}px;
  justify-content: center;
  align-items: center;
`;
