import React, { useState } from "react";
import {
  AuthWrap,
  AuthWrapFirst,
  AuthWrapSecond,
  AuthWrapThird,
  AuthWrapFourth,
  AuthLogo,
  ButtonAccept,
} from "./styled";
import global from "../../resources/global";
import {
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  View,
  ActivityIndicator,
} from "react-native";
import Logoauth from "../../assets/img/logoauth.svg";
import { Book13, SemiBold14 } from "../../resources/palettes";
import Inputs from "../../src/components/Input/Inputs";
import { useDispatch, useSelector } from "react-redux";
import { userLogin } from "../../src/redux/actions/authActions";

const AuthScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [phone, setPhone] = useState("990008991");
  const [password, setPassword] = useState("secret");
  const { error } = useSelector((state) => state.auth);
  const { loading } = useSelector((state) => state.loading);

  if (loading)
    return (
      <View style={{ alignItems: "center", flex: 1, justifyContent: "center" }}>
        <ActivityIndicator />
      </View>
    );
  const handleSubmit = () => {
    dispatch(userLogin({ phone, password, navigation }));
  };
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <AuthWrap>
        <Image
          source={global.images.auth}
          style={{
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: "100%",
            height: "100%",
            position: "absolute",
          }}
        />
        <AuthWrapFirst>
          <AuthLogo>
            <Logoauth />
          </AuthLogo>
        </AuthWrapFirst>
        <AuthWrapSecond>
          <Book13>Логин</Book13>
          <Inputs value={phone} onChangeText={setPhone} />
          <Book13>Пароль</Book13>
          <Inputs value={password} secured onChangeText={setPassword} />
        </AuthWrapSecond>
        <AuthWrapThird>
          {error && <Book13>{error.message}</Book13>}
          <SemiBold14>Войдя в аккаунт вы соглашаетесь с</SemiBold14>
          <SemiBold14 color={global.colors.yellow}>
            условиями использования приложения
          </SemiBold14>
        </AuthWrapThird>
        <AuthWrapFourth>
          <TouchableOpacity activeOpacity={0.9} onPress={() => handleSubmit()}>
            <ButtonAccept>
              <SemiBold14>Войти</SemiBold14>
            </ButtonAccept>
          </TouchableOpacity>
        </AuthWrapFourth>
      </AuthWrap>
    </TouchableWithoutFeedback>
  );
};

export default AuthScreen;
