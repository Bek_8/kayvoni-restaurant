import styled from "styled-components";
import global from "../../resources/global";

export const AuthWrap = styled.View`
  flex: 1;
`;

export const AuthWrapFirst = styled.View`
  flex: 3;
  justify-content: center;
  align-items: center;
`;

export const AuthWrapSecond = styled.View`
  flex: 2;
  padding-left: 10px;
`;

export const AuthWrapThird = styled.View`
  flex: 1;
  align-items: center;
`;

export const AuthWrapFourth = styled.View`
  flex: 1;
  align-items: center;
`;

export const AuthLogo = styled.View`
  border: 1px solid ${global.colors.white};
  background-color: ${global.colors.white};
  width: ${global.strings.width / 2}px;
  height: ${global.strings.width / 2}px;
  border-radius: 100px;
  align-items: center;
  justify-content: center;
`;

export const ButtonAccept = styled.View`
  width: ${global.strings.width - 30}px;
  height: ${global.strings.height / 23}px;
  border-radius: 15px;
  background-color: ${global.colors.green};
  justify-content: center;
  align-items: center;
`;
