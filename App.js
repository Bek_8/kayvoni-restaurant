import React, { useEffect, useState } from "react";
import { View, Text, TextInput } from "react-native";
import * as Font from "expo-font";
import AppContainer from "./src/Navigation/navigation";
import { Provider } from "react-redux";
import { store, persistor } from "./src/redux/store";
import { PersistGate } from "redux-persist/integration/react";

export default function App() {
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
  TextInput.defaultProps.allowFontScaling = false;
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    Font.loadAsync({
      Bold: require("./assets/fonts/bold.ttf"),
      Book: require("./assets/fonts/book.ttf"),
      SemiBold: require("./assets/fonts/semibold.ttf"),
    }).then(() => {
      setLoading(false);
    });
  }, []);
  if (loading)
    return (
      <View>
        <Text>Loading</Text>
      </View>
    );
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppContainer />
      </PersistGate>
    </Provider>
  );
}
