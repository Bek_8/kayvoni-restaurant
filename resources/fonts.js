const fonts = {
  book: "Book",
  bold: "Bold",
  semiBold: "SemiBold",
};

export default fonts;
