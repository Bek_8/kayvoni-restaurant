import colors from "./colors";
import images from "./images";
import strings from "./strings";
import fonts from "./fonts";
const global = {
  colors,
  images,
  strings,
  fonts,
};

export default global;
