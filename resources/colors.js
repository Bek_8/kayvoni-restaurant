const colors = {
  main: "#440000",
  white: "#FFFFFF",
  logoback: "#FFE7C3",
  yellow: "#FFD600",
  green: "#6FCF97",
  blue: "#2F80ED",
  black: "#333333",
  secondaryText: "#BABABA",
  red: "#EB5757",
};

export default colors;
